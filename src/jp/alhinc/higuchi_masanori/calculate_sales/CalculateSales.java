package jp.alhinc.higuchi_masanori.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class CalculateSales {
	public static void main(String[] args) {

		//支店定義ファイル読込用マップ作成(支店コード、支店名称)
		Map<String, String> branchMap = new TreeMap<>();

		//売上合計計算用マップ作成（支店コード、売上合計）
		Map<String, Long> totalMap = new TreeMap<>();

		// 支店定義ファイルを読み込む
		BufferedReader br = null;

		try {
			File file = new File(args[0], "branch.lst");
			br = new BufferedReader(new FileReader(file));

			//ファイルの存在チェック
			if (file.exists() == false) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			String line;
			while ((line = br.readLine()) != null) {

				//エラーチェック(支店コードが数字3桁かつ支店名称ありかどうか)
				if(line.matches("^[0-9]{3},.+") == false) {
					//エラー
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;

				}

				//支店コード、支店名称を取得
				String branchData[] = line.split(",", 0);

				//エラーチェック(項目数が2かどうか)
				if(branchData.length != 2) {
					//エラー
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				//マップに格納
				//支店コード、支店名称
				branchMap.put(branchData[0], branchData[1]);
				//支店コード、売上合計（初期値0）
				totalMap.put(branchData[0], 0L);
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return;
		} finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return;
				}

			}

		}

		//売上ファイルを読み込む→売上を加算

		//フォルダの売上ファイル（.rcdのファイル）のフィルタ作成
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File file, String str) {
				//拡張子(.rcd)、ファイル名が数字8桁を指定
				return str.matches("^[0-9]{8}.rcd$");

			}
		};

		//拡張子(.rcd)、ファイル名が数字8桁のファイルを抽出
		File[] files = new File(args[0]).listFiles(filter);

		Arrays.sort(files, Comparator.naturalOrder());

		//ファイル数分処理実行
		for (int i = 0; i < files.length; i++) {

			//エラーチェック(ファイル名が連番かどうか)
			if(i != 0) {

				Integer fname1 = Integer.parseInt(files[i-1].getName().substring(0, 8));
				Integer fname2 = Integer.parseInt(files[i].getName().substring(0, 8));

				if(fname1 != (fname2 - 1)) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}

			}

			try {
				FileReader fr = new FileReader(files[i]);
				br = new BufferedReader(fr);

				//1行目を取得(支店コード)
				String branchCd = br.readLine();

				if (branchCd == null) {
					//エラー
					System.out.println(files[i].getName() + "のフォーマットが不正です");
					return;
				}

				//エラーチェック（支店コードが数値3桁かどうか）
				if(branchCd.matches("^[0-9]{3}$") == false) {
					//エラー
					System.out.println(files[i].getName() + "の支店コードが不正です");
					return;
				}

				//2行目を取得(売上額)
				String sales = br.readLine();

				if (sales == null) {
					//エラー
					System.out.println(files[i].getName() + "のフォーマットが不正です");
					return;
				}
				//エラーチェック(売上額が数字10桁以内かどうか)
				if (sales.matches("^[0-9]{1,10}$") == false) {
					//エラー
					System.out.println("予期せぬエラーが発生しました");
					return;
				}


				//エラーチェック(売上ファイルが3行以上かどうか)
				if (br.readLine() != null) {
					//エラー
					System.out.println(files[i].getName() + "のフォーマットが不正です");
					return;
				}

				//支店コードが登録済みなら売上合計に加算
				if(totalMap.get(branchCd) == null) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				Long total = Long.parseLong(sales) + totalMap.get(branchCd);

				//エラーチェック(売上額が数字10桁以内かどうか)
				if (total.toString().length() > 10) {
					//エラー
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				totalMap.put(branchCd, total);

			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました。");
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました。");
						return;
					}

				}
			}
		}

		//ファイルに出力
		try {
			FileWriter f = new FileWriter(new File(args[0], "branch.out") ,false);
			PrintWriter p = new PrintWriter(new BufferedWriter(f));

			//キー情報の取得
			Set<String> mapset = branchMap.keySet();

			for(String key : mapset) {
				//[支店コード],[支店名称],[支店別売上合計]
				p.print(key + "," + branchMap.get(key) + "," + totalMap.get(key));
				//改行
				p.println();
			}
			p.close();

		}catch(IOException ex) {
			System.out.println("予期せぬエラーが発生しました。");
			return;
		}

	}
}
